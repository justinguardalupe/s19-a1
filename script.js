/*console.log("hello");*/
//==================> #3 Exponent
var getCube = Math.pow(2, 3);

//==================> #4 Template Literals
let message = `The cube of 2 is ${getCube}`;
console.log(message);

//==================> #5 #6

var address = ["258", "Washington Ave NW", "California", "90011"];

var [block, street, state, number] = address;
console.log(`I live at ${address[0]}, ${address[1]},
${address[2]}, ${address[3]}`);

//==================> #7 #8

var animal = {
	Name: "Lolong",
	Type: "saltwater crocodile",
	Weight: "1075 kgs",
	Meas: "20 ft 3 in"
}

var {Name, Type, Weight, Meas} = animal;

function getDetails ({Name, Type, Weight, Meas}){
	console.log(`${Name} was a ${Type}.
He weighed at ${Weight} with a measurement of ${Meas}.`);
}

getDetails(animal);

//==================> #7 #8
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = "5";
myDog.breed = "Miniature Dachshund";

console.log(myDog);